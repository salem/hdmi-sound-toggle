#!/bin/bash
USR=`whoami`
if [ $USR != "root" ]; then
	echo "You need to run this script with sudo!"
else
	cp "hdmi_sound_toggle" "/usr/local/bin/"
	chmod +x "/usr/local/bin/hdmi_sound_toggle"
	cp "hdmi_sound.rules" "/etc/udev/rules.d/"
	echo "Done!"
fi
